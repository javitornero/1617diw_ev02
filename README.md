#README / LEEME

*Proyecto:* 1617DIW_EV02

*Evaluación:* 2

*Módulo:* Diseño de Interfaces WEB

*Ciclo Formativo:* Desarrollo de Aplicaciones WEB

*Objetivo:* El rostro de Cervantes y su paso por la ciudad de Valencia

##Author / Autor

Javier Tornero Montellano (mail: [jtorne82@gmail.com](jtorne82@gmail.com) / twitter: [@jtorne82](https://twitter.com/jtorne82))

##License / Licencia

Creative Commons: [by-no-sa](http://es.creativecommons.org/blog/licencias/) 

Lacre:	https://commons.wikimedia.org/wiki/File:Lacre-JP-VII.jpg
Pluma:	https://pixabay.com/es/pluma-tintero-obsoleto-1300305/
Calle de Cervantes	https://commons.wikimedia.org/wiki/File:Calle_de_Cervantes_(Madrid).jpg
https://commons.wikimedia.org/wiki/File:Anagrama_oficial_de_%22palabra_sobre_palabra%22.jpg
This file is licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license.

    You are free:

        to share – to copy, distribute and transmit the work
        to remix – to adapt the work

    Under the following conditions:

        attribution – You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
        share alike – If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

Pluma cuervo  CC0 Public Domain
Gratis para usos comerciales
No es necesario reconocimiento 

https://pixabay.com/es/pluma-cuervo-negro-escribir-305300/

1613_cervantes_novelas_exemplares
https://upload.wikimedia.org/wikipedia/commons/9/90/1613_cervantes_novelas_exemplares.png

Textura
https://pixabay.com/p-1074136/?no_redirect
 CC0 Public Domain
Gratis para usos comerciales
No es necesario reconocimiento 

Lope de Vega
https://commons.wikimedia.org/wiki/File:LopedeVega.jpg

Textura firma Cervantes
https://c1.staticflickr.com/2/1598/26692601375_50a7016d45_b.jpg