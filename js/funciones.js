function acciones(section, accion){
        if(accion=='on'){
            $('#img--cap--small--'+section).removeClass('img--cap--reset').addClass('img--cap--accion');
            $('#img--cap--medium--'+section).removeClass('img--cap--reset').addClass('img--cap--accion');
            $('#flipper--text--'+section).removeClass('flipper--text--reset').addClass('flipper--text--accion');
            $('#flipper--portrait--'+section).removeClass('flipper--portrait--reset').addClass('flipper--portrait--accion');
            $('#text--'+section).removeClass('text--reset').addClass('text--accion');
            $('#img--zoom--'+section).removeClass('img--zoom--reset').addClass('img--zoom--accion'); 
            $('#img--lacre--'+section).removeClass('img--lacre--reset').addClass('img--lacre--accion');                
        }
        if(accion=='reset'){
            setTimeout(function(){
                $('#img--cap--small--'+section).removeClass('img--cap--accion').addClass('img--cap--reset');
                $('#img--cap--medium--'+section).removeClass('img--cap--accion').addClass('img--cap--reset');
                $('#flipper--text--'+section).addClass('flipper--text--reset');
                $('#flipper--portrait--'+section).addClass('flipper--portrait--reset');
                $('#text--'+section).removeClass('text--accion').addClass('text--reset');
                $('#img--zoom--'+section).removeClass('img--zoom--accion').addClass('img--zoom--reset');
                $('#img--lacre--'+section).removeClass('img--lacre--accion').addClass('img--lacre--reset'); 
            }, 10);
        }
}
function controlDiv(section, mitadVentana, mitadVentanaNeg, topOfWindow, porcentaje){
    var divAnimaPos = $('#cont--ar--'+section).offset().top;
    var dif=parseInt(divAnimaPos)-parseInt(topOfWindow);

    if (-350<dif && dif<mitadVentana+porcentaje) {
        acciones(section, "on");
        $('#cont--ar--'+section).removeClass( "desaparece" ).addClass("aparece");
    }else{
        acciones(section, "reset");
        $('#cont--ar--'+section).removeClass( "aparece" ).addClass("desaparece");
    }
}

$(document).ready(function() {
    $(window).scroll(function() {
        var altoVentana=$( window ).height();
        var mitadVentana= altoVentana/2;
        var mitadVentanaNeg= mitadVentana*(-1);
        var topOfWindow = $(window).scrollTop();
        //alert(altoVentana);
        var porcentaje=altoVentana*25/100; // Aquí regulas el momento en e que aparece
        var porcentajeNeg=porcentaje*(-1);

        var sectionsQty=$('div[id^=cont--ar--]').length; // Identifica cuantos elementos quiero espiar
        
        for(var i=1; i<=sectionsQty; i++){
            controlDiv(i, mitadVentana, mitadVentanaNeg, topOfWindow, porcentaje);
        }
    });
    
    // Animación para ir suavemente a los enlaces de la barra y desde abajo hacia arriba
    $(".navbar a, footer a[href='#home']").on('click', function (event) {

        // En caso de que exista para de ancla (#) en la referencia
        if (this.hash !== "") {

            event.preventDefault();

            // guardo el valor del ancla
            var hash = this.hash;

            // Animo el scroll con jQuery
            $('html, body').animate({

                scrollTop: $(hash).offset().top - 40

            }, 900, function () {

                // Cuando acabe la animación referencio la ubicacion de la página en el ancla almacenado
                window.location.hash = hash;

            });
        }
    });
});
	



